libtk-img for Debian
--------------------

Basic notes on the use of this package:

tclsh% package require Img
will load the Img library. Once loaded, it adds format handlers so that
[create image photo -format foo] can handle more formats. Specifically,
Img provides support for bmp, gif, jpeg, png, tiff, xbm, xpm, postscript,
and pdf image formats.

For more information on the image handlers and usage, see
/usr/share/doc/libtk-img/README.

--

Another point of interest may be the extreme amounts of hacking done to
the source tree. Upstream includes their own copies of libjpeg, libpng,
libtiff, and libz, to be used in case they're missing on the system. I've
excised these as much as possible, so the source tarball isn't exactly
pristine.

The principal exception to this pruning is a bunch of libtiff header files
needed for the tiff support.

 -- Mike Markley <mike@markley.org>, Tue, 12 Jun 2001 18:34:47 -0700
